#include <iostream>
#include <stdio.h>
#include <fstream>
#include <string>
#include <algorithm>
#include <vector>

std::vector<std::string> prologue;
std::vector<std::string> epilogue;

/*
Read operations from an input file and return all operations
*/
std::vector<std::string> readInput(const char* fname)
{
    std::string str;
    std::vector<std::string> operations;

    std::ifstream filestream(fname);

    while (std::getline(filestream, str))
    {
        if (epilogue.empty()) {
            std::size_t pos = str.find("\tc0    ");
            if (pos == 0) {
                operations.push_back(str);
            } else {
                pos = str.find(";;");
                if (pos == 0)
                    operations.push_back(str);
                else {
                    pos = str.find(".call printf");
                    if (pos == 0) {
                        epilogue.push_back(str);
                    } else {
                        if (!operations.empty()) {
		            copy(operations.begin(), operations.end(), back_inserter(prologue));
                            operations.clear();
                        }
                        prologue.push_back(str);
                    }
                }
            }
        } else {
            epilogue.push_back(str);
        }
    }

   return operations;
}

/*
Print scheduled VLIW operations to a file.
*/
void printOutput(const char* fname, std::vector<std::string> scheduledVLIW)
{
    std::ofstream outfile;
    outfile.open(fname);

    for (int i = 0; i < prologue.size(); i++)
        outfile << prologue[i] << "\n";

    for (int i = 0; i < scheduledVLIW.size(); i++)
        outfile << scheduledVLIW[i] << "\n";

    for (int i = 0; i < epilogue.size(); i++)
        outfile << epilogue[i] << "\n";

    outfile.close();
}

/*
TODO : Write any helper functions that you may need here.

*/

#include "vex.h"

struct Dep {
    Op * predecessor;
    Op * successor;
    int kind;
    int getDist() {return (*predecessor).latency;}
};

str toString(Dep edge) {
    return (*edge.predecessor).repr.substr(1) + " --> " + (*edge.successor).repr.substr(1);
}

std::vector<Dep> findRAW(std::vector<Op> operations, int * D) {
    /* for p in readfrom(G, n2): # p is things n2 reads from
                if p in writeto(G, n1): # if p is writen to by n1
                    RAW.add_edge(n1, n2)
                    D.add_edge(n1, n2, label="RAW")
                    break */
    // Initialize output variable
    std::vector<Dep> out;
    const int N = operations.size();
    
    // Main loop of all permutations between operations
    // Since Depenencies only depend on operations after some operation
    // the last member is not included for n1.
    for (std::vector<Op>::size_type n1 = 0; n1 < N-1; n1++) {

        // And n2 starts after n1, Such that n2 > n1
        for (std::vector<Op>::size_type n2 = n1+1; n2 < N; n2++) {

            // Iterating through each register p n2 reads from
            std::set<std::string> * readsfrom = &operations[n2].readsfrom;
            std::set<std::string>::iterator ptr;
            for (ptr = (*readsfrom).begin(); ptr != (*readsfrom).end(); ++ptr) {

                // if p is written to by operation n1
                if (operations[n1].writesto.count(*ptr) != 0) {

                    // Create new dependency
                    Dep e = Dep();
                    e.predecessor = &operations[n1];
                    e.successor   = &operations[n2];
                    e.kind        = RAW;
                    D[n1 + n2*N] = e.getDist();

                    // Add to the output a new dependency
                    out.push_back(e);
                    break;
                }
            }
        }
    }

    // return output
    return out;
}

std::vector<Dep> findWAR(std::vector<Op> operations, int * D) {
    /* for p in writeto(G, n2): # p is things n2 writes to
                if p in readfrom(G, n1): # if p is read by n1
                    WAR.add_edge(n1, n2)
                    if (not nx.has_path(RAW, n1,n2)) or (not singular):
                        D.add_edge(n1, n2, label="WAR")
                    break */
    // Initialize output variable
    std::vector<Dep> out;
    const int N = operations.size();

    // Main loop of all permutations between operations
    // Since Depenencies only depend on operations after some operation
    // the last member is not included for n1.
    for (std::vector<Op>::size_type n1 = 0; n1 < N-1; n1++) {

        // And n2 starts after n1, Such that n2 > n1
        for (std::vector<Op>::size_type n2 = n1+1; n2 < N; n2++) {

            // Iterating through each register p n2 writes to
            std::set<std::string> * writesto = &operations[n2].writesto;
            std::set<std::string>::iterator ptr;
            for (ptr = (*writesto).begin(); ptr != (*writesto).end(); ++ptr) {

                // if p is read by operation n1
                if (operations[n1].readsfrom.count(*ptr) != 0) {

                    // Create new dependency
                    Dep e = Dep();
                    e.predecessor = &operations[n1];
                    e.successor   = &operations[n2];
                    e.kind        = WAR;
                    D[n1 + n2*N] = e.getDist();

                    // Add to the output a new dependency
                    out.push_back(e);
                    break;
                }
            }
        }
    }

    // return output
    return out;
}

std::vector<Dep> findWAW(std::vector<Op> operations, int * D) {
    /* for p in writeto(G, n2): # p is things n2 writes to
                if p in writeto(G, n1): # if p is writen to by n1
                    WAW.add_edge(n1, n2)
                    if (not nx.has_path(RAW, n1, n2)) or (not singular):
                        D.add_edge(n1, n2, label="WAW")
                    break */
    // Initialize output variable
    std::vector<Dep> out;
    const int N = operations.size();

    // Main loop of all permutations between operations
    // Since Depenencies only depend on operations after some operation
    // the last member is not included for n1.
    for (std::vector<Op>::size_type n1 = 0; n1 < N-1; n1++) {

        // And n2 starts after n1, Such that n2 > n1
        for (std::vector<Op>::size_type n2 = n1+1; n2 < N; n2++) {

            // Iterating through each register p n2 writes to
            std::set<std::string> * writesto = &operations[n2].writesto;
            std::set<std::string>::iterator ptr;
            for (ptr = (*writesto).begin(); ptr != (*writesto).end(); ++ptr) {

                // if p is written to by operation n1
                if (operations[n1].writesto.count(*ptr) != 0) {

                    // Create new dependency
                    // n2 depends on n1
                    Dep e = Dep();
                    e.predecessor = &operations[n1];
                    e.successor   = &operations[n2];
                    e.kind        = WAW;
                    D[n1 + n2*N] = e.getDist();
                    

                    // Add to the output a new dependency
                    out.push_back(e);
                    break;
                }
            }
        }
    }

    // return output
    return out;
}

// Get's the height of a node, where NxN is the size of the dependency matrix D
int height(int n, int * D, const int N) {
    // WARNING: D must be topologically sorted
    int max = 0;
    
    // get the maximum from recursion of all linked nodes
    for (int m = n; m<N; m++) { // Since it's topologically sorted already
        // If there's a link, return 1 + the height of m, else return 0
        if (D[n + m*N] >= 0) {  // If there's an edge
            int out = 1 + height(m,D,N);
            
            // If the height is a maximum, return it
            if (out > max) {max = out;}
        }
    }
    
    return max;
}
void testHeight(int * D, const int N) {
    std::cout << "========= Test Height =========\n";
    for (int i = 0; i < N; i++)
        std::cout << height(i, D, N) << ", ";
    std::cout << "\n";
}

// Get's the fanout of a node, where NxN is the size of the dependency matrix D
int fanOut(int n, int * D, const int N) {
    // WARNING: D must be topologically sorted
    int out = 0;
    
    // get the maximum from recursion of all linked nodes
    for (int m = n; m<N; m++) { // Since it's topologically sorted already
        // If there's a link, return 1 + the height of m, else return 0
        if (D[n + m*N] >= 0) {  // If there's an edge
            out += 1;
        }
    }
    
    return out;
}
void testFanOut(int * D, const int N) {
    std::cout << "========= Test FanOut =========\n";
    for (int i = 0; i < N; i++)
        std::cout << fanOut(i, D, N) << ", ";
    std::cout << "\n";
}

// Returns the priority of a resource given a set of operations
float resourcePriority(std::vector<Op> ops, const int opType) {
    int out = 0;
    for (int n = 0; n < ops.size(); n++) {
        if (ops[n].type == opType) {out++;}
    }
    return ((float)out)/((float)getMaxN(opType));
}

std::vector<float> priority(std::vector<Op> ops, int * D, bool heuristic) {
    const int N = ops.size();
    const float segV = N + 1;
    std::vector<float> out;
    out.reserve(N);
    
    // Calculate resource priority
    int resPTable[10];
    if (!heuristic) {
        resPTable[TYPE_MEMORY_LOAD] = resourcePriority(ops,TYPE_MEMORY_LOAD);
        resPTable[TYPE_MEMORY_STORE] = resourcePriority(ops,TYPE_MEMORY_STORE);
        resPTable[TYPE_INTEGER] = resourcePriority(ops,TYPE_INTEGER);
        resPTable[TYPE_INTEGER_MUL] = resourcePriority(ops,TYPE_INTEGER_MUL);
        resPTable[TYPE_CONTROL] = resourcePriority(ops,TYPE_CONTROL);
        resPTable[TYPE_INTERCLUSTER] = resourcePriority(ops, TYPE_INTERCLUSTER);
        resPTable[TYPE_NOP] = resourcePriority(ops, TYPE_NOP);
    }
    
    // Calculate values
    for (int n = 0; n<N; n++) {
        float temp =  height(n,D,N);                 // use height as primary
        if(!heuristic) {
            temp += (float)resPTable[ops[n].type] / segV;   // use resP as secondary
        } else {
            temp += ((float)fanOut(n,D,N)) / ((float)N);
        }
        out.push_back(temp); //out[n] += (N-n) / segV / segV;         // then use order as tertiary
    }
    
    return out;   
}
std::vector<float> priority(std::vector<Op> ops, int * D) {return priority(ops,D,false);}

// Return's whether item at index currLoc would remain topological in D at newLoc
bool isTopological(int currLoc, int newLoc, int * D, const int N) {
    for (int n2 = 0; n2<=newLoc; n2++) {
        if (D[currLoc + n2*N]>=0) {return false;}
    }
    for (int n2 = N-1; n2<=newLoc; n2--) {
        if (D[currLoc + n2*N]>=0) {return false;}
    }
    return true;
    
}

void testIsTopological(int * D, const int N){
    std::cout << "========= Test isTopological =========" << "\n";
    for (int currLoc = 0; currLoc<N; currLoc++) {
        int maxVal = 0;
        for (int newLoc = 0; newLoc<N; newLoc++) {
            if (isTopological(currLoc,newLoc,D,N)) {maxVal = newLoc;}
        }
        std::cout << maxVal << ", ";
    }
    std::cout << "\n";
    
    for (int currLoc = N-1; currLoc>=0; currLoc--) {
        int minVal = 0;
        for (int newLoc = N-1; newLoc>=0; newLoc--) {
            if (isTopological(currLoc,newLoc,D,N)) {minVal = newLoc;}
        }
        std::cout << minVal << ", ";
    }
    std::cout << "\n";
}

/*
// WARNING: ops and priorities should be the same length.
std::vector<Op> priorityTopologicalSort(std::vector<Op> ops, std::vector<float> priorities, int * D) {
    // Set Variables
    const int N = ops.size();
    
    // Used to store whether or not an index has been sorted already
    bool setIndex[N];
    for(int i = 0; i<N; i++) {setIndex[i]=false;} // Initialize to false

    // The output
    std::vector<Op> out;
    out.reserve(N);
    
    // Main loop
    for (int n1 = 0; n1<N; n1++) {
        int maxidx = -1;
        float maxval = -1;
        for (int n2 = 0; n2<N; n2++) {
            if (!setIndex[n2]) {                // As long as we haven't set anything yet
                if (priorities[n2]>maxval) {    // If the priority is greater than the max valid one we've seen
                    // Now we need to check if it's topological, meaning no node n2 <= n1 exists where  >= n1 can exist.
                    bool check = isTopological(n2, n1, D, N);
                    if (check) {          // If it's topologically valid
                        maxidx = n2;
                        maxval = priorities[n2];
                    }
                }
            }
        }
        // set our output
        
        setIndex[maxidx] = true;
        out.push_back(ops[maxidx]);
    }
    std::cout << "Here4";
    return out;
}
*/

int getPossibleForN(std::vector<Op> ops, int n, int * R, int S[], int * D, const int N) {
    // get the earliest time n could execute given it's predecessors execution time
    int s = 0;                          // n's earliest execution time
    for (int m = 0; m<n; m++) {         // for all predecessors
        int latency = D[m + n*N];
        if (latency >= 0) {             // If there's an edge
            if(S[m]>=0) {
                int d = S[m] + latency;
                if (d>s) {s=d;}             // max operation
            } else {
                return -1;
            }
        }
    }
    
    /* Delay the instruction further until the needed
       resources are available. */
    int type = getType(ops[n].opcode);
    while(R[type + s*10] <= 0 || R[TYPE_END + s*10] <= 0) {
        s++;
    }
        
    return s;
}

std::vector<int> getAllowed(std::vector<Op> ops, int s, int * R, int S[], int * D, const int N) {
    //std::cout << "\nAllowed: ";
    std::vector<int> out;
    for (int n = 0; n<N; n++) {
        if (S[n]<0) {
            if (getPossibleForN(ops,n,R,S,D,N)==s) {
                out.push_back(n);
                //std::cout << n << ", ";
            }
        }
    }
    //std::cout << "\n";
    return out;
}

int getBest(std::vector<int> allowed, std::vector<float> priorities, const int N) {
    // get the max priority
    float maxPriority = 0;
    for (int i = 0; i<allowed.size(); i++) {
        float p = priorities[allowed[i]];
        if (p>maxPriority) {
            maxPriority = p;
        }
    }
    //std::cout << "Max Priority: " << maxPriority << "\n";
    
    // Get anything that equals the max
    std::vector<int> bestSoFar;
    for (int i = 0; i<allowed.size(); i++) {
        int temp = allowed[i];
        if (priorities[temp]==maxPriority) {
            bestSoFar.push_back(temp);
        }
    }
    //std::cout << "Best So Far: " << bestSoFar.size() << "\n";
    
    // If only one has priority, return it
    if (bestSoFar.size()==1) {
        //std::cout << "Best: " << bestSoFar[0];
        return bestSoFar[0];
    } else {
        // Otherwise, return the minimum index
        int minval = N;
        for (int i = 0; i<bestSoFar.size(); i++) {
            if (bestSoFar[i]<minval) {minval=bestSoFar[i];}
        }
        //std::cout << "Best: " << minval;
        return minval;
    }
}

/*
Input : std::vector<std::string> operations. The input is a vector of strings. Each string in the vector is an operation in the original vex code.

Returns : std::vector<std::string>

The function should return a vector of strings. Each string should be a scheduled operation or ;; that marks the end of a VLIW operation word.
*/
std::vector<std::string> scheduleVLIW(std::vector<std::string> operations)
{
    std::vector<std::string> scheduledVLIW;

    /* TODO : Implement your code here */

    // Print operations
    printVec(operations);
    test_vexh();

    // Convert to operations and print
    std::vector<Op> ops;
    std::cout << "\n" << "============ Operations ============";
    for (int n = 0; n < operations.size(); n++) {
        Op op (operations[n], ops.size()+1);
        if (getType(op.opcode) != TYPE_END) {
            std::cout << n << ": " << toString(&op) << " <-- " << operations[n].substr(1) << "\n";
            ops.push_back(op);
        }
    }
    
    // Create dependency matrix
    int N = ops.size();
    int DM[N][N];
    
    // Initialize to negative
    for (int n1 = 0; n1<N; n1++)
        for (int n2=0; n2<N; n2++)
            DM[n1][n2] = -1;
    
    // Get RAW dependencies and print
    std::vector<Dep> raw = findRAW(ops, &DM[0][0]);
    std::cout << "\n" << "============ RAW Depenencies ============" << "\n";
    for (int n = 0; n < raw.size(); n++)
        std::cout << toString(raw[n]) << "\n";
        
    // Get WAR dependencies and print
    std::vector<Dep> war = findWAR(ops, &DM[0][0]);
    std::cout << "\n" << "============ WAR Depenencies ============" << "\n";
    for (int n = 0; n < war.size(); n++)
        std::cout << toString(war[n]) << "\n";
        
    // Get WAW dependencies and print
    std::vector<Dep> waw = findWAW(ops, &DM[0][0]);
    std::cout << "\n" << "============ WAW Depenencies ============" << "\n";
    for (int n = 0; n < waw.size(); n++)
        std::cout << toString(waw[n]) << "\n";
    
    // Print 2D array
    std::cout << "\n" << "============ Depenency Matrix ============" << "\n";
    for (int n1 = 0; n1<N; n1++) {
        for (int n2=0; n2<N-1; n2++) {
            printf("%02d, ",(&DM[0][0])[n1+n2*N]);
        }
        printf("%d\n",(&DM[0][0])[n1+(N-1)*N]);
    }
    std::cout << "\n";
    
    // Test methods
    testHeight(&DM[0][0], N);
    testIsTopological(&DM[0][0], N);
    testFanOut(&DM[0][0], N);
    
    // Prioritization
    std::cout << "\n" << "============ Prioritization ============" << "\n";
    std::vector<float> priorities = priority(ops, &(DM[0][0]), 1);
    std::cout << priorities.size();
    //std::cout << "\n" << "============ Sorting ============" << "\n";
    //ops = priorityTopologicalSort(ops, priorities, &(DM[0][0]));
    
    // List Scheduling algorithm
    // Create schedule and empty reservation table
    std::cout << "\n" << "============ Begin List Schedule ============" << "\n";
    int S[N], RT[N][10];
    for (int n = 0; n<N; n++) {
        S[n] = -1;
        RT[n][TYPE_INTEGER] = N_INTEGER;
        RT[n][TYPE_INTEGER_MUL] = N_INTEGER_MUL;
        RT[n][TYPE_MEMORY_LOAD] = N_MEMORY_LOAD;
        RT[n][TYPE_MEMORY_STORE] = N_MEMORY_STORE;
        RT[n][TYPE_CONTROL] = 1;
        RT[n][TYPE_INTERCLUSTER] = 1;
        RT[n][TYPE_NOP] = WIDTH;
        RT[n][TYPE_END] = WIDTH; // Use this for everything
    }
        
    // Main loop
    
    /*for (int n = 0; n<N; n++) {
        int s = getPossibleForN(ops, n, &(RT[0][0]), S, &(DM[0][0]), N);
        int type = ops[n].type;
            
        // Set S and subtract a resource from the resource table
        S[n] = s;
        RT[s][type]--;
        RT[s][TYPE_END]--;
    } */
    
    int scheduled = 0;
    int s = 0;
    while (scheduled<N) {
        std::vector<int> allowed = getAllowed(ops, s, &(RT[0][0]), S, &(DM[0][0]), N);
        while (allowed.size()!=0) {
            // Get best from among the allowed
            int best = getBest(allowed, priorities, N);
            int type = ops[best].type;
            
            // Set S and subtract a resource from the resource table
            S[best] = s;
            RT[s][type]--;
            RT[s][TYPE_END]--;
            scheduled++;
            
            allowed = getAllowed(ops, s, &(RT[0][0]), S, &(DM[0][0]), N); // recalculate
        }
        s++;
    }
    
    // Put them in order
    std::cout << "\n" << "============ scheduleVLIW ============" << "\n";
    scheduledVLIW.push_back(operations[0]);
    scheduledVLIW.push_back(";;");
    for(int PC = 0; PC<s; PC++) {
        for (int n = 1; n<N; n++) {
            if (PC==S[n]) {
                str out = ops[n].repr;
                scheduledVLIW.push_back(out);
                std::cout << out << "\n";
            }
        }
        for (int n = 0; n<RT[PC][TYPE_END]; n++) {
            str out = "\tc0    NOP";
            //scheduledVLIW.push_back(out);
            //std::cout << out << "\n";
        }
        scheduledVLIW.push_back(";;");
        std::cout << ";;\n";
    }
    
    
    return scheduledVLIW;
}

int main(int argc, char *argv[])
{

   if(argc != 2) {
       std::cout << "Invalid parameters \n";
       std::cout << "Expected use ./vliwScheduler <input file name>\n";
   }

   const char* inputFile = argv[1];
   const char* vliwSchedulerOutput = "4_wide.s";

   std::vector<std::string> operations;
   std::vector<std::string> scheduledVLIW;

   /* Read operations from the file */
   operations = readInput(inputFile);

   /* Schedule operations */
   scheduledVLIW = scheduleVLIW(operations);

   /* Print scheduled operations to file */
   printOutput(vliwSchedulerOutput, scheduledVLIW);
}
