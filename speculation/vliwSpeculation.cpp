#include <iostream>
#include <stdio.h>
#include <fstream>
#include <string>
#include <algorithm>
#include <vector>

std::vector<std::string> prologue;
std::vector<std::string> epilogue;

/*
Read instructions from an input file and return all instructions 
*/
std::vector<std::string> readInput(const char* fname)
{
    std::string str;
    std::vector<std::string> instructions;
    
    std::ifstream filestream(fname);
    
    while (std::getline(filestream, str))
    {
        if (epilogue.empty()) {
            std::size_t pos = str.find("\tc0    ");
            if (pos == 0) {
                instructions.push_back(str);
            } else {
                pos = str.find(";;");
                if (pos == 0)
                    instructions.push_back(str);
                else {
                    pos = str.find(".call printf");
                    if (pos == 0) {
                        epilogue.push_back(str);
                    } else {
                        if (!instructions.empty()) {
		            copy(instructions.begin(), instructions.end(), back_inserter(prologue));
                            instructions.clear();
                        }
                        prologue.push_back(str);
                    }
                }
            }
        } else {
            epilogue.push_back(str);
        }
    }
   
   return instructions;
}

/*
Print scheduled VLIW instructions to a file.
*/
void printOutput(const char* fname, std::vector<std::string> scheduledVLIW)
{
    std::ofstream outfile;
    outfile.open(fname);
  
    for (int i = 0; i < prologue.size(); i++)
        outfile << prologue[i] << "\n";

    for (int i = 0; i < scheduledVLIW.size(); i++)
        outfile << scheduledVLIW[i] << "\n";

    for (int i = 0; i < epilogue.size(); i++)
        outfile << epilogue[i] << "\n";

    outfile.close();
}

/*
TODO : Write any helper functions that you may need here. 

*/


/*
Input : std::vector<std::string> instructions. The input is a vector of strings. Each string in the vector is an instruction in the original vex code.

Returns : std::vector<std::string>

The function should return a vector of strings. Each string should be a scheduled instruction or ;; that marks the end of a VLIW instruction word.
*/
std::vector<std::string>  scheduleVLIW(std::vector<std::string> instructions)
{
    std::vector<std::string> scheduledVLIW;

    /* TODO : Implement your code here */

    return scheduledVLIW;
}

int main(int argc, char *argv[])
{

   if(argc != 2) {
       std::cout << "Invalid parameters \n";
       std::cout << "Expected use ./vliwSpeculation <input file name>\n";
   }
 
   const char* inputFile = argv[1];
   const char* vliwSpeculationOutput = "4_wide.s";

   std::vector<std::string> instructions;
   std::vector<std::string> scheduledVLIW;
 
   /* Read instructions from the file */
   instructions = readInput(inputFile);

   /* Schedule instructions */
   scheduledVLIW = scheduleVLIW(instructions);

   /* Print scheduled instructions to file */
   printOutput(vliwSpeculationOutput, scheduledVLIW);
}
