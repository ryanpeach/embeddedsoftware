#include <set>
#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <stack>

typedef std::vector<std::string> strvec;
typedef std::string str;

enum { RAW, WAW, WAR };

const str MEMORY = "M*";
const str NOPCODE = "NOP";

// Prints a vector of strings with new lines between them
void printVec(strvec v) {
  std::cout << "printVec ---------- \n";
  for(strvec::size_type n = 0; n < v.size(); n++) {
    std::cout << v[n] << "\n";
  }
  std::cout << "Done ---------- \n";
}

// Creates a binary string from an unsigned integer
// REF: http://interactivepython.org/runestone/static/pythonds/BasicDS/ConvertingDecimalNumberstoBinaryNumbers.html
str toBinaryString(size_t x, size_t size){
  size_t dec = x;
  std::stack<double> rems;
  while (dec > 0) {
    double rem = dec % 2;
    rems.push(rem);
    dec /= 2;
  }
  
  str out;
  
  // leading zeros
  while ((out.size()+rems.size())<size) {
    out += "0";
  }
  
  // number
  while (!rems.empty()) {
    out += (bool)rems.top() ? "1" : "0";
    rems.pop();
  }
  
  return out;
}

/* Finds string s in vector v */
int vfind(strvec v, str s) {
  for (int i = 0; i<v.size(); i++){
    if (v[i]==s) { return i; }
  }
  return -1;
}

/* converts several types to strings */
str toString(std::set<std::string> op) {
  str out = "{";
  for (std::set<std::string>::iterator n = op.begin(); n != op.end(); ++n) {
    out.append(*n);
    if (n != --op.end()) {out.append(", ");}
  }
  out.append("}");
  return out;
}

str toString(strvec op) {
  str out = "{";
  if (op.size()>0) {
    out.append(op[0]);
    for (strvec::iterator n = ++op.begin(); n != op.end(); ++n) {
      out.append(", ");
      out.append(*n);
    }
  }
  out.append("}");
  return out;
}

str toString(strvec op, str del) {
  str out = "";
  if (op.size()>0) {
    out.append(op[0]);
    for (strvec::iterator n = ++op.begin(); n != op.end(); ++n) {
      out.append(del);
      out.append(*n);
    }
  }
  return out;
}

// /* checks if two string vectors are the same */
// bool vecEquals(strvec a, strvec b) {
//   if (a.size() == b.size()) {
//     for (strvec::size_type i = 0; i < a.size(); i++) {
//       if (a[i] != b[i]) {return false;}
//     }
//     return true; // if not returned yet
//   } else {
//     return false; // if not the same size
//   }
// }

// How I used to think substring worked
// Defines beginning position and b as 1+last position
str strslice(str s, int a, int b) {
  return s.substr(a,b-a);
}

// maps a string-to-string function onto a vector of strings
strvec stringMap(strvec v, str (*ex)(str)) {
  std::cout << "vRegEx ---------- \n";
  strvec out;

  // For each operation, apply a regex search
  for(strvec::size_type n = 0; n < v.size(); n++) {
    out.push_back(ex(v[n]));
  }

  return out;
}

// Checks if anything in X is equal to y
template<typename T>
bool any_equals(T y, std::vector<T> X) {
  for (std::size_t i = 0; i<X.size(); i++) {
    if(y==X[i]) { return true; }
  }
  return false;
}

// splits on any in a set of delimeters, keeps the delimeter if keep is true
strvec split(str s, std::vector<char> dels, bool keep){
  strvec out;
  int n0 = 0;

  for (int n1 = 0; n1<s.size(); n1++) {
    if (any_equals(s[n1],dels) && !any_equals(s[n0],dels)) {
      out.push_back(strslice(s,n0,n1));
      n0 = n1;
    } else if (n1==s.size()-1 && n0 != n1) {
      out.push_back(s.substr(n0));
      break;
    } else if (!keep && any_equals(s[n0],dels)) {
      n0 = n1;
    }
  }

  return out;
}
strvec split(str s, std::vector<char> dels) {return split(s,dels,true);}

// Splits a string on these delimeters: comma, tab, and space
strvec splitsp(str s, bool keep) {
  std::vector<char> dels;
  dels.push_back(' ');
  dels.push_back('\t');
  dels.push_back(',');
  return split(s, dels, keep);
}
strvec splitsp(str s){return splitsp(s,false);}

bool isEmpty(strvec s) {
  return s.size()==0 || s[0] == "##" || s[0] == "#";
}

bool isEmpty(str s) {
  strvec test = splitsp(s);
  return isEmpty(test);
}

bool isEndInst(strvec s) {return s[0]==";;";}
bool isEndInst(str s) {return isEndInst(splitsp(s));}
bool isMemory(str s) {return s[0]=='0';}
bool isRegister(str s) {return s[0]=='$';}
bool isChannel(str s) {return s[0]=='c';}
int getChannelNum(str s) {
  s = s.substr(1);
  int out;
  std::istringstream(s) >> out;
  return out;
}
bool isNoOp(str s) {return s.find(NOPCODE)!=str::npos;}
bool isNoOp(strvec op) {return isNoOp(op[0]);}
str getChannel(strvec op) {return op[0];}
str getOpCode(strvec op) {return op[1];}
str getMemValue(str s) {
  int n0, n1;
  std::size_t find2 = s.find("(");
  std::size_t find3 = s.find(")");
  std::size_t find4 = s.find("[");
  std::size_t find5 = s.find("]");

  if (find2 != str::npos) {n0 = find2;}
  else if (find4 != str::npos) {n0 = find4;}
  else {return "";}

  if (find3 != str::npos) {n1 = find3;}
  else if (find5 != str::npos) {n1 = find5;}
  else {return "";}

  return strslice(s, n0+1, n1);
}

str getMemOffset(str s) {
  return s.substr(0, std::max(s.find('('),s.find('[')));
}
// ----------- Vectored Functions -----------
// Finds some character in s other than the character tofind, return's -1 otherwise
int findNot(str s, char tofind) {
  int out = -1;
  for (int n0 = 0; n0 < s.size(); n0++) {
    if (s[n0] != tofind) {
      out = n0;
      break;
    }
  }
  return out;
}

// Get the channel inside an operation
// returns "c#" if found, "" otherwise
str getChannel(str inst) {
  int n0 = inst.find('c');       // Channels start with the letter c before anything else in an operation
  if (n0 >= 0) {                 // If it exists
    int n1 = inst.substr(n0).find(' ');
    if (n1 >= 0) {
      return inst.substr(n0,n1);  // Return the substring assuming it's a single digit channel
    }
  }
  return "";
}

// Returns whether or not a string is a noop
// Returns ";;" if found, "" otherwise
str getNoOp(str inst) {
  int c = inst.find(NOPCODE);
  if (c >= 0) {
    return inst.substr(c,c+NOPCODE.size());
  } else {
    return "";
  }
}

// Returns the op of a string
str getOpCode(str inst) {
  // Find the channel
  int n0 = inst.find("c");
  if (n0 >= 0) {
    inst = inst.substr(n0); // Remove everything before that character

    // Now find the end of the channel
    int n1 = inst.find(" ");
    if (n1 >= 0) {
      inst = inst.substr(n1);

      // Now find the first character
      int n2 = findNot(inst, ' ');
      if (n2 >= 0) {
        inst = inst.substr(n2);

        // Now find the first space
        int n3 = inst.find(" ");
        if (n3 >= 0) {
          return inst.substr(0,n3); // The final value is between the character and the next space
        }
      }
    }
  }
  return ""; // Return empty string if nothing found
}

// Get's the next register in a string
str getNext(str inst) {
  int n0 = findNot(inst,' ');
  if (n0 >= 0) {
    inst = inst.substr(n0);     // Append string to start of register
    int n1 = inst.find(" ");    // Find the next space (assuming well formatted code)
    if (n1 >= 0) {              // If there is a next space
      return inst.substr(0,n1); // Return appended to next space
    }
    return inst; // Return appended to the end
  }
  return "";    // Return empty if none found
}

str getAfter(str inst, str x) {
  std::size_t idx = inst.find(x);
  if (idx != str::npos && idx+x.size()<inst.size()) {
    return inst.substr(idx+x.size());
  } else {return "";}
}
// Get's the next register in a string
str getNextX(str inst, str x) {
  int n0 = inst.find(x);
  if (n0 >= 0) {
    inst = inst.substr(n0);   // Append string to start of register
    int n1 = inst.find(" ");  // Find the next space (assuming well formatted code)
    return inst.substr(0,n1); // Return appended to next space
  }
  return "";    // Return empty if none found
}


str getNextRegister(str inst) {
  return getNextX(inst, "$");
}

str getNextMemory(str inst) {
  return getNextX(inst, "0");
}

// Get's the comments
str getComments(str inst) {
  int n0 = inst.find("##");
  if (n0 >= 0) {
    return inst.substr(n0);   // Everything after ## is a comment
  }
  return "";    // Return empty if none found
}

// Now for the structures

bool hasReadWrite(strvec op) {
  int equalsIdx = vfind(op,"=");
  return equalsIdx >= 0;
}


std::set<std::string> getWrites(strvec op) {
  std::set<std::string> out;
  int equalsIdx = vfind(op,"=");
  if (equalsIdx >= 0) {
    str writes = op[equalsIdx-1];
    out.insert(writes);
    if (isMemory(writes)){
      out.insert(MEMORY);
    }
  }

  return out;
}

std::set<std::string> getReads(strvec op) {
  std::set<std::string> out;
  int equalsIdx = vfind(op,"=");
  if (equalsIdx >= 0) {
    int commentsIdx = vfind(op,"##");
    if (commentsIdx == -1) {
      commentsIdx = op.size();
    }

    // Left of the equals is a read if it's a memory
    str writes = op[equalsIdx-1];
    if (isMemory(writes)){
      out.insert(getMemValue(writes));
    }

    // Everything right of the equals
    if (equalsIdx >= 0) {
      for (int i = equalsIdx+1; i<commentsIdx; i++) {
        str reads = op[i];
        out.insert(reads);
        if (isMemory(reads)){
          out.insert(getMemValue(reads));
          out.insert(MEMORY);
        }
      }
    }
  }

  return out;
}

struct Op {
    strvec                rep;
    std::string           opcode;
    std::string           channel;
    std::set<std::string> writesto;
    std::set<std::string> readsfrom;
    float                 latency;

    // Constructor
    Op(strvec op, bool strict) {init(op, strict);}
    Op(strvec op) {init(op, true);}
    Op(std::string op, bool strict) { init(splitsp(op), strict); } // Just construct from the split
    Op(std::string op) {init(splitsp(op), true);}

  private:
    void init(strvec op, bool strict) {                 // TODO: Make constructor from string
      rep = op;
      if (op[0]!=";;") {
        opcode = getOpCode(op);
        channel = getChannel(op);
        writesto = getWrites(op);
        readsfrom = getReads(op);
      } else {opcode = ";;";}
    }
};

str toString(Op * op) {
  str out = "{Channel: ";
  out.append((*op).channel);
  out.append(", OpCode: ");
  out.append((*op).opcode);
  out.append(", WritesTo: ");
  out.append(toString((*op).writesto));
  out.append(", ReadsFrom: ");
  out.append(toString((*op).readsfrom));
  out.append("}");
  return out;
}

typedef std::vector<Op> Instr;
typedef std::vector<Instr> Vex;

// Takes a set of op strings and divides them by instruction
std::vector<strvec> toInstructions(strvec ops) {
  std::vector<strvec> out;
  strvec inst;
  for (int i = 0; i < ops.size(); i++) {
    bool isEnd = isEndInst(ops[i]);
    if (!isEnd && !isEmpty(ops[i])) {
      inst.push_back(ops[i]);
    } else if(isEnd) {
      inst.push_back(ops[i]);
      out.push_back(inst);
      inst.clear();
    }
  }
  return out;
}

/* Test Methods */
bool testToStringVec() {
  strvec a;
  bool test1 = toString(a)=="{}";
  a.push_back("1");
  a.push_back("38lslsd");
  bool test2 = toString(a)=="{1, 38lslsd}";
  return test1 && test2;
}

bool testToStringDel() {
  strvec a;
  bool test1 = toString(a, " ")=="";
  a.push_back("1");
  a.push_back("38lslsd");
  bool test2 = toString(a, " ")=="1 38lslsd";
  return test1 && test2;
}

bool testVFind(){
  strvec a;
  a.push_back("1");
  a.push_back("38lslsd");
  bool test1 = vfind(a, "") == -1;
  bool test2 = vfind(a, "1") == 0;
  bool test3 = vfind(a, "38lslsd") == 1;
  return test1 && test2 && test3;
}

// bool testVecEquals() {
//   strvec a;
//   strvec b;
//   bool test1 = a==b;
//   a.push_back("1");
//   a.push_back("38lslsd");
//   b.push_back("1");
//   b.push_back("38lslsd");
//   bool test2 = a==b;
//   return test1 && test2;
// }

bool testStrSlice(){
  return (strslice("1234",0,1)=="1") &&
          (strslice("1234",1,3)=="23");
}

bool testSplitSp() {
  strvec out1 = splitsp("   	  123 4,  56 ");
  bool test1 = (out1.size() == 3) &&
                out1[0] == "123" &&
                out1[1] == "4" &&
                out1[2] == "56";
  if (!test1) {std::cout << "testSplitSp test1 failed...";}

  strvec out2 = splitsp("   	  123 4,  56");
  bool test2 = (out2.size() == 3) &&
                out2[0] == "123" &&
                out2[1] == "4" &&
                out2[2] == "56";
  if (!test2) {std::cout << "testSplitSp test2 failed...";}

  strvec out3 = splitsp("123 4,  56");
  bool test3 = (out3.size() == 3) &&
                out3[0] == "123" &&
                out3[1] == "4" &&
                out3[2] == "56";
  if (!test3) {std::cout << "testSplitSp test3 failed...";}

  strvec out4 = splitsp("123");
  bool test4 = (out4.size() == 1) &&
                out4[0] == "123";
  if (!test4) {std::cout << "testSplitSp test4 failed...";}

  strvec out5 = splitsp("");
  bool test5 = (out5.size() == 0);
  if (!test5) {std::cout << "testSplitSp test5 failed...";}

  return test1 && test2 && test3 && test4 && test5;
}

bool testIsEmpty() {
  bool test1 = isEmpty("           # asdf ");
  if (!test1) {cout<<"test1 failed"; return false;}
  bool test2 = isEmpty("           ## asdf ");
  if (!test2) {cout<<"test2 failed"; return false;}
  bool test3 = !isEmpty("            asdf ");
  if (!test3) {cout<<"test3 failed"; return false;}
  bool test4 = !isEmpty("  ;;         # asdf ");
  if (!test4) {cout<<"test4 failed"; return false;}
  bool test5 = !isEmpty("  c0 and         # asdf ");
  if (!test5) {cout<<"test5 failed"; return false;}
  return true;
}

bool testGetMemValue() {
  str out1 = "$r0.1";
  str val1 = getMemValue("0x5($r0.1)");
  bool test1 = val1==out1;
  if (!test1) {std::cout << "testGetMemValue test1 failed... " << val1; return false;}

  out1 = "$r0.1";
  val1 = getMemValue("0x5[$r0.1]");
  test1 = val1==out1;
  if (!test1) {std::cout << "testGetMemValue test1 failed... " << val1; return false;}

  out1 = "1";
  val1 = getMemValue("0x5[1)");
  test1 = val1==out1;
  if (!test1) {std::cout << "testGetMemValue test1 failed... " << val1; return false;}

  out1 = "";
  val1 = getMemValue("0x5()");
  test1 = val1==out1;
  if (!test1) {std::cout << "testGetMemValue test1 failed... " << val1; return false;}

  out1 = "";
  val1 = getMemValue("");
  test1 = val1==out1;
  if (!test1) {std::cout << "testGetMemValue test1 failed... " << val1; return false;}

  return true;
}

bool testFindNot() {
  return (findNot("    c0     add...", ' ')==4) &&
         (findNot("        ", ' ')==-1);
}

bool testGetChannel() {
  return (getChannel("      c0     add...")=="c0") &&
         (getChannel("      c10     add...")=="c10") &&
         (getChannel("    add ")=="");
}

bool testGetNoOp() {
  return (getNoOp("      c0     add...")=="") &&
         (getNoOp(NOPCODE)==NOPCODE);
}

bool testGetOp() {
  return (getOpCode("      c0     add ...")=="add") &&
         (getOpCode(NOPCODE)=="");
}

bool testGetNext() {
  return (getNext("        c0       add $r1 = $r2, $r3")=="c0") &&
         (getNext("        c")=="c") &&
         (getNext("        ")=="") &&
         (getNext("c        ")=="c");
}

bool testGetNextX() {
  return (getNextX("        c0       add $r1 = $r2, $r3", "$")=="$r1");
}

bool testGetComments() {
  return (getComments("        c0       add $r1 = $r2, $r3 ##asdf f s sd")=="##asdf f s sd") &&
         (getComments("        c0       add $r1 = $r2, $r3 ")=="");
}

bool testhasReadWrite() {
  bool test1 = hasReadWrite(splitsp("	c0    add $r0.1 = $r0.1, (-0x80)"));
  bool test2 = !hasReadWrite(splitsp("	c0    NOP"));
  return test1 && test2;
}

bool testGetWrites() {
  std::set<str> out1;
  out1.insert("$r0.1");
  bool test1 = getWrites(splitsp("	c0    add $r0.1 = $r0.1, (-0x80)"))==out1;
  if (!test1) {std::cout << "testGetWrites test1 failed...";}

  std::set<str> out2;
  out2.insert("0x01($r0.1)");
  out2.insert(MEMORY);
  std::set<str> val2 = getWrites(splitsp("	c0    add 0x01($r0.1) = $r0.1, (-0x80)"));
  bool test2 = val2==out2;
  if (!test2) {std::cout << "testGetWrites test2 failed... ";}

  std::set<str> out3;
  std::set<str> val3 = getWrites(splitsp("	c0    NOP"));
  bool test3 = val3==out3;
  if (!test3) {std::cout << "testGetWrites test3 failed... ";}

  return test1 && test2 && test3;
}

bool testGetReads() {
  std::set<str> out1;
  out1.insert("$r0.1");
  out1.insert("$r0.2");
  out1.insert("(-0x80)");
  std::set<str> val1 = getReads(splitsp("	c0    add 0x01($r0.1) = $r0.2, (-0x80)"));
  bool test1 = val1==out1;
  if (!test1) {std::cout << "testGetReads test1 failed... " << toString(val1);}

  std::set<str> out2;
  out2.insert("$r0.1");
  out2.insert(MEMORY);
  out2.insert("(-0x80)");
  out2.insert("0x01($r0.1)");
  std::set<str> val2 = getReads(splitsp("	c0    add $r0.1 = 0x01($r0.1), (-0x80) ## asdf"));
  bool test2 = val2==out2;
  if (!test2) {std::cout << "testGetReads test2 failed... " << toString(val2);}

  std::set<str> out3;
  std::set<str> val3 = getReads(splitsp("	c0    NOP"));
  bool test3 = val3==out3;
  if (!test3) {std::cout << "testGetReads test3 failed... ";}

  return test1 && test2 && test3;
}

bool testToInstructions() {
  strvec testcode;
  testcode.push_back("	c0    NOP");
  testcode.push_back("	c0    NOP");
  testcode.push_back("	c0    mov $r0.7 = $r0.0   ## bblock 0, line 11,  t21,  0(SI32)");
  testcode.push_back("	c0    cmpne $b0.0 = $r0.3, $r0.0   ## bblock 0, line 16,  t69(I1),  t20,  0(SI32)");
  testcode.push_back(";;								## 0");
  testcode.push_back("	c0    mov $r0.9 = 2   ## [spec] bblock 4, line 0,  t49,  2(I32)");
  testcode.push_back("	c0    mov $r0.6 = 2   ## [spec] bblock 4, line 0,  t45,  2(I32)");
  testcode.push_back("	c0    mov $r0.8 = 3   ## [spec] bblock 4, line 0,  t50,  3(I32)");
  testcode.push_back("	c0    mov $r0.4 = 3   ## [spec] bblock 4, line 0,  t46,  3(I32)");
  testcode.push_back(";;		");
  std::vector<strvec> inst = toInstructions(testcode);
  bool test1 = inst[0][0]==testcode[0];
  bool test2 = inst[0][4]==testcode[4];
  bool test3 = inst[1][0]==testcode[5];
  bool test4 = inst[1][4]==testcode[9];
  bool test5 = inst[0].size()==5;
  bool test6 = inst[1].size()==5;
  bool test7 = inst.size()==2;
  if(!test1) {std::cout << "test1 failed. " << inst[0][0]; return false;}
  if(!test2) {std::cout << "test2 failed. " << inst[0][4]; return false;}
  if(!test3) {std::cout << "test3 failed. " << inst[1][0]; return false;}
  if(!test4) {std::cout << "test4 failed. " << inst[1][4]; return false;}
  if(!test5) {std::cout << "test5 failed. " << inst[1][4]; return false;}
  if(!test6) {std::cout << "test6 failed. " << inst[1][4]; return false;}
  if(!test7) {std::cout << "test7 failed. " << inst[1][4]; return false;}
  return true;
}

// A general purpose test method
void test_vexh() {
  std::cout << "Testing: \n";
  std::cout << "testGetChannel:\t" << testGetChannel() << "\n";
  std::cout << "testGetNoOp:\t" << testGetNoOp() << "\n";
  std::cout << "testFindNot:\t" << testFindNot() << "\n";
  std::cout << "testGetOp:\t" << testGetOp() << "\n";
  std::cout << "testGetNext:\t" << testGetNext() << "\n";
  std::cout << "testGetNextX:\t" << testGetNextX() << "\n";
  std::cout << "testGetComments:\t" << testGetComments() << "\n";
  std::cout << "testSplitSp:\t" << testSplitSp() << "\n";
  std::cout << "testStrSlice:\t" << testStrSlice() << "\n";
  //std::cout << "testVecEquals:\t" << testVecEquals() << "\n";
  std::cout << "testToStringVec:\t" << testToStringVec() << "\n";
  std::cout << "testToStringDel:\t" << testToStringDel() << "\n";
  std::cout << "testVFind:\t" << testVFind() << "\n";
  std::cout << "testhasReadWrite:\t" << testhasReadWrite() << "\n";
  std::cout << "testGetWrites:\t" << testGetWrites() << "\n";
  std::cout << "testGetReads:\t" << testGetReads() << "\n";
  std::cout << "testGetMemValue:\t" << testGetMemValue() << "\n";
  std::cout << "testIsEmpty:\t" << testIsEmpty() << "\n";
  std::cout << "testToInstructions:\t" << testToInstructions() << "\n";

  strvec testv;
  testv.push_back("	c0    add $r0.1 = $r0.1, (-0x80)");
  testv.push_back("	c0    NOP");
  testv.push_back("	c0    mov $r0.3 = $r0.1   ## addr(_?1STRINGVAR.1)(P32)");
  testv.push_back(";;								## 0");

  for (int i = 0; i<testv.size(); i++) {
    Op op (testv[i]);
    cout << testv[i] << "\n";
    cout << toString(&op) << "\n";
  }
}
