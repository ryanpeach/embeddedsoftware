#include <iostream>
#include <stdio.h>
#include <fstream>
#include "vliwEncoder.h"

#define MAX_INSTR_WIDTH 100

/*
Read instructions from an input file and return a char array
*/
std::vector<std::string> readInput(const char* fname)
{
    std::string str;
    std::vector<std::string> instructions;

    std::ifstream filestream(fname);

    while (std::getline(filestream, str))
    {
        int current_position = 0;
        std::size_t pos = str.find("\tc0    ");
        if (pos == 0)
        {
            instructions.push_back(str);
        } else {
            std::size_t f = str.find(";;");
            if (f == 0)
                instructions.push_back(str);
        }
    }

   return instructions;
}

/*
Print an encoded VLIW instruction to a file. The encoded instruction should be a character array and terminated by '\0' character
*/
void printOutput(const char* fname, std::vector<std::string> encodedVLIW)
{
    std::ofstream outfile;
    outfile.open(fname);

    for(int i = 0; i < encodedVLIW.size(); i++)
        outfile << encodedVLIW[i] << "\n";

    outfile.close();
}

/*
TODO : Write any helper functions that you may need here.

*/
#include"vex.h"

const int bsyllable = 32;
const int bmask = 1;

str maskString(bool mask[], size_t width) {
    str out;
    for (int i = 0; i<width; i++) {
        out = out + ((mask[i]) ? "1" : "0");
    }
    return out;
}

int bmaskafter = 0;
strvec generateMask(strvec inst) {
    int width = inst.size() - 1;
    bool mask[width];

    // Generate Mask
    for (int i = 0; i<width; i++) {
        mask[i] = !isNoOp(inst[i]);
        bmaskafter += bmask;
    }
    
    // Generate output encoding in one line
    strvec out;
    str ch   = getChannel(inst[0]);
    str temp = maskString(mask, width);
    str encoding = ch+"\t"+temp;
    for (int i = 0; i<width; i++){
        if(!isNoOp(inst[i])) {
            str data = getAfter(inst[i],ch); // get after channel
            data = data.substr(4); // get after the next 4 spaces
            /*
            // remove comments
            str::size_type commentsidx = data.find("#");
            if (commentsidx != str::npos) {
                data = strslice(data,0,commentsidx); // get after the next 4 spaces until comment
            }
            
            // remove trailing whitespace
            size_t last = data.size()-1;
            while (data[last]==' ') {last--;}
            data = strslice(data,0,last+1);
            */
            // Create encoding
            encoding += "\t" + data;
            bmaskafter += bsyllable;
        }
    }
    
    //if(encoding.size()>MAX_INSTR_WIDTH) {cout << "WARNING, too long.\n";}
    out.push_back(encoding);    // push back the encoding
    out.push_back(inst[width]); // push back last line
    return out;
}

/*
Input : std::vector<std::string> instructions. The input is a vector of strings. Each string in the vector is one line in the vex code. A line can be a instruction or a ';;'

Returns : std::vector<std::string>

The function should return a vector of strings. Each string should be a line of VLIW encoded instruction with masked encoding scheme
*/
std::vector<std::string>  maskedVLIW(std::vector<std::string> instructions)
{
    std::vector<std::string> encodedVLIW;
    int bbefore = 0; // used to calcuate output sizes

    /* TODO : Implement your code here */
    std::vector<strvec> insts = toInstructions(instructions);
    for (int i = 0; i<insts.size(); i++) {
        bbefore += (insts[i].size()-1)*bsyllable; // simply equals the number of syllables times bsyllable
        strvec gen = generateMask(insts[i]);
        
        // Print gen to output
        for (int j = 0; j < gen.size(); j++) {
            encodedVLIW.push_back(gen[j]);
        }
    }
    
    cout << "Masked Size (bits) Before: " << bbefore << "\n";
    cout << "Masked Size (bits) After: " << bmaskafter << "\n";
    return encodedVLIW;
}

/*
Input : std::vector<std::string> instructions. The input is a vector of strings. Each string in the vector is one line in the vex code. A line can be a instruction or a ';;'

Returns : std::vector<std::string>

The function should return a vector of strings. Each string should be a line of VLIW encoded instruction with template encoding scheme
*/

#include <bitset>
#include <math.h>
typedef pair<int,int> edge;
str templateString(vector<edge> tmp, int size) {
    str out;
    for (int i = 0; i<tmp.size(); i++) {
        edge ei = tmp[i];
        str val = toBinaryString(ei.second-ei.first, size);
        out += val;
    }
    return str(out);
}

int btempafter = 0;
strvec generateTemplate(strvec inst) {
    int width = inst.size() - 1;
    int tmpsize = ceil(log(width)/log(2));
    int n0 = 0;
    std::vector<edge> tmp;
    
    // Get the edges for tmp
    for (int i = 0; i<width; i++) {
        if(!isNoOp(inst[i])){
            edge op;
            op.first = n0;
            op.second = i;
            tmp.push_back(op);
            n0++;
            btempafter += tmpsize*bmask;
        }
    }
    
    // Generate output encoding in one line
    strvec out;
    str ch   = getChannel(inst[0]);
    str temp = templateString(tmp, tmpsize);
    str encoding = ch+"\t"+temp;
    for (int i = 0; i<width; i++){
        if(!isNoOp(inst[i])) {
            str data = getAfter(inst[i],ch); // get after channel
            data = data.substr(4); // get after the next 4 spaces
            
            /*// remove comments
            str::size_type commentsidx = data.find("#");
            if (commentsidx != str::npos) {
                data = strslice(data,0,commentsidx); // get after the next 4 spaces until comment
            }
            
            // remove trailing whitespace
            size_t last = data.size()-1;
            while (data[last]==' ') {last--;}
            data = strslice(data,0,last+1);
            */
            // Create encoding
            encoding += "\t" + data;
            btempafter += bsyllable;
        }
    }
    
    //if(encoding.size()>MAX_INSTR_WIDTH) {cout << "WARNING, too long.\n";}
    out.push_back(encoding);    // push back the encoding
    out.push_back(inst[width]); // push back last line
    return out;
}

std::vector<std::string>  templateVLIW(std::vector<std::string> instructions)
{
    std::vector<std::string> encodedVLIW;
    int bbefore = 0;
    
    /* TODO : Implement your code here */
    std::vector<strvec> insts = toInstructions(instructions);
    for (int i = 0; i<insts.size(); i++) {
        bbefore += (insts[i].size()-1)*bsyllable; // simply equals the number of syllables times bsyllable
        strvec gen = generateTemplate(insts[i]);
        
        // Print gen to output
        for (int j = 0; j < gen.size(); j++) {
            encodedVLIW.push_back(gen[j]);
        }
    }
    
    cout << "Template Size (bits) Before: " << bbefore << "\n";
    cout << "Template Size (bits) After: " << btempafter << "\n";
    return encodedVLIW;
}


int main(int argc, char *argv[])
{
   if(argc != 2) {
       std::cout << "Invalid parameters \n";
       std::cout << "Expected use ./vliwEncoder <input file name>\n";
   }

   const char* inputFile = argv[1];
   const char* maskedOutput = "maskedEncoding.txt";
   const char* templateOutput = "templateEncoding.txt";

   std::vector<std::string> instructions;
   std::vector<std::string> maskedEncoding;
   std::vector<std::string> templateEncoding;

   /* Read instructions from the file */
   instructions = readInput(inputFile);
   
   /* Encode instructions using masked and template encoding */
   maskedEncoding = maskedVLIW(instructions);
   templateEncoding = templateVLIW(instructions);

   /* Print encoded instructions to file */
   printOutput(maskedOutput,maskedEncoding);
   printOutput(templateOutput,templateEncoding);
}
