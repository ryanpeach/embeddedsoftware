import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import networkx as nx
from networkx.drawing.nx_pydot import write_dot
from subprocess import call
import re, os, datetime, string
import itertools as it

# Dependency graph heuristics
writeto = lambda g, x: g.successors(x)
readfrom = lambda g, x: g.predecessors(x)

def checkdir(x):
    if not os.path.exists(x):
        os.makedirs(x)
        
def check_type(x, t=int):
    try:
        t(x)
        return True
    except:
        return False
    
def remove_offset(x):
    # Type checking
    if not isinstance(x,str):
        return x
    if len(x) < 6: # Things which can't have two brackets, a number, and a register
        return x
        
    # Test if the first character is a number
    if not check_type(x[0], t=int):
        return x
        
    # Test that the second and last characters are brackets
    if not (x[1] in {"[", "("} and x[-1] in {"]", ")"}):
        return x
        
    # Return the remainder
    return x[2:-1]

def remove_extra_nodes(G):
    """ Removes any nodes from G which have no edges. """
    out = G.copy()
    for n in G.nodes_iter():
        if len(writeto(G,n)) == 0 and len(readfrom(G,n)) == 0:
            out.remove_node(n)
    return out
    
def save_graph(G, outpath, fname="graph", LR=True):
    
    # Change to left-right orientation
    if LR:
        if "graph" in G.graph:
            G.graph['graph']['rankdir'] = 'LR'
        else:
            G.graph['graph'] = {'rankdir': 'LR'}
        
    # Get some filename and path values            
    name = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    outpath = outpath+name+"/"
    fout = outpath+fname+".dot"
    
    # Create the path
    checkdir(outpath)
        
    # Plot and save
    write_dot(G,fout)
    call(["dot", "-Tpng", fout, "-o", outpath+fname+".png"])
    call(["dot", "-Tpdf", fout, "-o", outpath+fname+".pdf"])
        
def full_graph(fpath, outpath = "./output/full/", save = True, LR = True):
    # Create Graph
    G = nx.DiGraph()
    
    # Open File and Iterate over it.
    with open(fpath, 'r') as f:
        for name, line in enumerate(f):
            # Remove certain characters
            for s in [',','\n']:
                line = line.replace(s,'')
                
            # Split by spaces
            values = line.split(" ")   # Split by space into a list
            
            # Initialize variables
            read = set()
            write = set()
                
            # Check Format and Process
            if "=" in values:          # This is if we are using the equals formatting stuff
                if len(values) == 5:
                    cmd, towrite, _, toread1, toread2 = values
                    toread3 = remove_offset(toread2)
                    toread4 = remove_offset(towrite)
                    read.add(toread1)
                    read.add(toread2)
                    read.add(toread3)
                    if toread4 != towrite:
                        read.add(toread4)
                    write.add(towrite)
                    
                elif len(values) == 4: # For things like ldw
                    cmd, towrite, _, toread = values
                    toread2 = remove_offset(toread)
                    toread3 = remove_offset(towrite)
                    read.add(toread)
                    read.add(toread2)
                    if toread3 != towrite:
                        read.add(toread3)
                    write.add(towrite)
                
            else:
                raise TypeError("Unable to read format's without = sign's yet.")  # TODO
                
            # Add to graph
            name = string.ascii_uppercase[name]
            G.add_node(name, label=name)
            
            for r in read:
                if not check_type(r, t=int):
                    G.add_node(r, label=r)
                    G.add_edge(r, name)
                
            for w in write:
                G.add_node(w, label=w)
                G.add_edge(name, w)
    
    if save:
        save_graph(G, outpath, LR=LR)
        
    return G
    
def dep_graph(G, outpath = "./output/dep/", save = True, LR = True, singular=False):
    # Create Graph
    D = nx.MultiDiGraph()
    
    # Add all instructions in G to D
    for n in G.nodes_iter():
        if n in string.ascii_uppercase: # If the node is an instruction
            D.add_node(n, label=n)
    
    RAW, WAR, WAW = D.copy(), D.copy(), D.copy()
    
    # Look for all RAW dependencies in G
    for n1, n2 in it.permutations(D.nodes(),2):
        # n2 is after n1
        if string.ascii_uppercase.index(n1) > string.ascii_uppercase.index(n1):
            
            # RAW Dependency: RAW happens when n2 reads from something n1 writes to
            for p in readfrom(G, n2): # p is things n2 reads from
                if p in writeto(G, n1): # if p is writen to by n1
                    RAW.add_edge(n1, n2)
                    D.add_edge(n1, n2, label="RAW")
                    break
    
            # WAR Dependency: WAR happens when n2 writes to something n1 reads from
            for p in writeto(G, n2): # p is things n2 writes to
                if p in readfrom(G, n1): # if p is read by n1
                    WAR.add_edge(n1, n2)
                    if (not nx.has_path(RAW, n1,n2)) or (not singular):
                        D.add_edge(n1, n2, label="WAR")
                    break
    
            # WAW Dependency: WAW happens when n2 writes to something n1 writes to
            for p in writeto(G, n2): # p is things n2 writes to
                if p in writeto(G, n1): # if p is writen to by n1
                    WAW.add_edge(n1, n2)
                    if (not nx.has_path(RAW, n1, n2)) or (not singular):
                        D.add_edge(n1, n2, label="WAW")
                    break
    
    # Save Graph
    if save:
        save_graph(remove_extra_nodes(RAW), outpath, fname="RAW", LR=LR)
        save_graph(remove_extra_nodes(WAR), outpath, fname="WAR", LR=LR)
        save_graph(remove_extra_nodes(WAW), outpath, fname="WAW", LR=LR)
        save_graph(D, outpath, LR=LR)
        
    return D
        
if __name__=="__main__":
    # Get arguments
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("fpath", help="The file path of the file to graph.")
    parser.add_argument("-o", "--outpath", help="The output file path of the dot file.", default="./output/")
    parser.add_argument("-v", "--vertical", help="Chooses to display graph vertically", action="store_true")
    args = parser.parse_args()
    fpath = args.fpath
    outpath = args.outpath
    LR = not args.vertical
    
    G = full_graph(fpath = fpath, outpath = outpath+"full/", save=True, LR = LR)
    D = dep_graph(G, outpath = outpath+"dependency/", save=True, LR = LR)